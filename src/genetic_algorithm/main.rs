extern crate mpi;
extern crate rand;

use rand::rngs::SmallRng;
use rand::{thread_rng, Rng};

use mpi::topology::Communicator;

use std::fmt;

const FIB: usize = 30;

const ITEM_COUNT: usize = 290;
const BIN_COUNT: usize = 200;
const POPULATION: usize = 550;
const COPY: usize = 50;
const COMBINE: usize = 450;

const RANDOM: usize = POPULATION - COMBINE - COPY;
const ITERATIONS: usize = 500;

#[derive(Copy)]
struct Chromosome {
    fit: u32,
    random: bool,
    genes: [u8; BIN_COUNT],
}

impl Chromosome {
    fn new(fit: u32, random: bool, genes: [u8; BIN_COUNT]) -> Chromosome {
        Chromosome {
            fit: fit,
            random: random,
            genes: genes,
        }
    }

    fn random() -> Chromosome {
        let mut genes = [0; BIN_COUNT];
        thread_rng().fill(&mut genes[..]);
        Chromosome {
            fit: 0,
            random: true,
            genes: genes,
        }
    }

    fn empty() -> Chromosome {
        Chromosome::new(0, false, [0; BIN_COUNT])
    }

    fn combine(a: &Chromosome, b: &Chromosome) -> Chromosome {
        let mut genes = [0_u8; BIN_COUNT];
        let mut rng = thread_rng();
        for i in 0..BIN_COUNT {
            genes[i] = if (rng.gen::<u8>() & 1) == 0 {
                a.genes[i]
            } else {
                b.genes[i]
            };
        }
        Chromosome::new(0, false, genes)
    }

    fn mutate(&mut self) {
        let mut rng = thread_rng();
        for i in 0..ITEM_COUNT {
            if rng.gen_bool(0.02) {
                self.genes[i] = rng.gen();
            }
        }
    }

    fn fitness(&mut self, vbins: &[u32; BIN_COUNT], vitems: &[u32; ITEM_COUNT]) {
        self.fit = 0;
        let mut bins = [0_u32; BIN_COUNT];
        let mut i = 0;
        while i < ITEM_COUNT {
            if bins[self.genes[i] as usize] + vitems[i] < vbins[self.genes[i] as usize] {
                bins[self.genes[i] as usize] += vitems[i];
                i += 1;
            } else {
                self.genes[i] = (self.genes[i] + 1) % BIN_COUNT as u8;
            }
        }

        for i in 0..BIN_COUNT {
            if bins[i] > vbins[i] {
                self.fit += 1 + (bins[i] - vbins[i]);
            } else if bins[i] > 0 {
                self.fit += 1;
            }
        }
    }
}

impl Eq for Chromosome {}

impl PartialEq for Chromosome {
    fn eq(&self, other: &Chromosome) -> bool {
        self.fit == other.fit && self.random == other.random && self.genes.eq(&other.genes[..])
    }
}

impl Clone for Chromosome {
    fn clone(&self) -> Chromosome {
        Chromosome::new(self.fit, self.random, self.genes)
    }
}

impl fmt::Display for Chromosome {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Chromosome ({}, {}, [{}, {}, {}, ..., {}])",
            self.fit,
            self.random,
            self.genes[0],
            self.genes[1],
            self.genes[2],
            self.genes[BIN_COUNT - 1],
        )
    }
}

fn spent_time(a: usize) -> usize {
    let n = if a < FIB { a } else { FIB };
    if n > 2 {
        spent_time(n - 1) + spent_time(n - 2)
    } else {
        n
    }
}

fn best_of(population: &mut [Chromosome; POPULATION]) -> usize {
    let mut index = thread_rng().gen_range(0, POPULATION);
    for _ in 0..5 {
        let i = thread_rng().gen_range(0, POPULATION);
        if population[i].fit < population[index].fit {
            index = i;
        }
    }
    index
}

#[inline]
fn own_size(mpi_rank: usize, mpi_size: usize, total: usize, max_size: usize) -> usize {
    total / mpi_size + (mpi_rank < total % mpi_size) as usize
}


fn genetic(vbins: &[u32; BIN_COUNT], vitems: &[u32; ITEM_COUNT],
        world: mpi::topology::SystemCommunicator) {
    let mpi_size = world.size() as usize;
    let mpi_rank = world.rank() as usize;

    let max_chunk_size = (POPULATION + mpi_size - 1) / mpi_size; // round up
    let mut chunk_sizes = vec![0; mpi_size];
    let mut chunk_offsets = vec![0; mpi_size];

    let max_cora_size = (COMBINE + RANDOM + mpi_size - 1) / mpi_size;
    let mut cora_sizes = vec![0; mpi_size];
    let mut cora_offsets = vec![0; mpi_size];

    {
        let mut chunk_offset = 0;
        let mut comb_rand_offset = 0;
        for i in 0..mpi_size {
            chunk_sizes[i] = own_size(i, mpi_size, POPULATION, max_chunk_size);
            chunk_offsets[i] = chunk_offset;
            chunk_offset += chunk_sizes[i];

            cora_sizes[i] = own_size(i, mpi_size, COMBINE + RANDOM, max_cora_size);
            cora_offsets[i] = comb_rand_offset;
            comb_rand_offset += cora_sizes[i];
        }
    }

    let mut population = [Chromosome::empty(); POPULATION];
    let mut old_population = [Chromosome::empty(); POPULATION];

    for i in chunk_offsets[mpi_rank]..chunk_offsets[mpi_rank] + chunk_sizes[mpi_rank] {
        population[i] = Chromosome::random();
        population[i].fitness(vbins, vitems);
    }

    // SHARE!

}

fn main() {
    let world = mpi::initialize().unwrap();

    let c = Chromosome::random();
    println!("{}", c);
}
