#[inline]
pub fn i32_to_bytes(value: i32, buffer: &mut [u8]) {
    buffer[0] = value as u8;
    buffer[1] = (value >> 8) as u8;
    buffer[2] = (value >> 16) as u8;
    buffer[3] = (value >> 24) as u8;
}

#[inline]
pub fn i32_from_bytes(bytes: &[u8]) -> i32 {
    bytes[0] as i32 | (bytes[1] as i32) << 8 | (bytes[2] as i32) << 16 | (bytes[3] as i32) << 24
}

#[inline]
pub fn bit_at(bytes: &[u8], i: usize) -> bool {
    bytes[i / 8] & (0b1000_0000 >> (i % 8)) != 0
}
