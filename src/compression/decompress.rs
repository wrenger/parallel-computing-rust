extern crate mpi;

use self::mpi::topology::{Communicator, SystemCommunicator};
use self::mpi::traits::{Destination, Source};

use std::fs::File;
use std::io::{Read, Write};

use super::bytes::i32_from_bytes;
use super::tree::Tree;

pub fn manager(f_in: &str, f_out: &str, world: SystemCommunicator) {
    let time_start = mpi::time();

    let time_header = mpi::time();
    let mut file_in = File::open(f_in).expect("open input: {}");
    let mut file_out = File::create(f_out).expect("create output: {}");

    let mut count_buffer = [0_u8; 8];
    file_in.read(&mut count_buffer).expect("read header: {}");
    let chunk_count = i32_from_bytes(&count_buffer) as usize;
    assert_eq!(chunk_count, world.size() as usize - 1);

    let mut header = vec![0_u8; chunk_count * 4];
    file_in.read(&mut header).expect("read header: {}");
    let mut input_sizes = vec![0; chunk_count];
    let mut max_input_size = 0;
    for i in 0..chunk_count {
        input_sizes[i] = i32_from_bytes(&header[i * 4..]) as usize;
        if input_sizes[i] > max_input_size {
            max_input_size = input_sizes[i];
        }
    }
    println!("time header: {:.6}", mpi::time() - time_header);

    let time_read =mpi::time();
    let mut input_buffer = vec![0_u8; max_input_size];
    for i in 0..chunk_count {
        file_in
            .read(&mut input_buffer[0..input_sizes[i]])
            .expect("read comp: {}");
        world
            .process_at_rank(i as i32 + 1)
            .send(&input_buffer[0..input_sizes[i]])
    }
    println!("time read: {:.6}", mpi::time() - time_read);

    let time_dec = mpi::time();
    let mut output_buffers = vec![vec![]; chunk_count];
    for _ in 0..chunk_count {
        let (output_buffer, status) = world.any_process().receive_vec::<u8>();
        let chunk = status.source_rank() as usize - 1;
        output_buffers[chunk] = output_buffer;
    }
    println!("time dec: {:.6}", mpi::time() - time_dec);

    let time_write = mpi::time();
    for output in output_buffers {
        file_out.write(&output).expect("write dec: {}");
    }
    println!("time write: {:.6}", mpi::time() - time_write);

    println!("time decompression: {:.6}", mpi::time() - time_start);
}

pub fn worker(tree: Tree, world: SystemCommunicator) {
    let (input, _status) = world.process_at_rank(0).receive_vec::<u8>();

    let output = decode(tree, &input);

    world.process_at_rank(0).send(&output[..]);
}

fn decode(tree: Tree, input: &[u8]) -> Vec<u8> {
    let mut output = Vec::with_capacity(input.len());

    let mut i = 0;

    loop {
        let (value, offset) = tree.value_of(input, i);
        if value == '@' {
            break;
        }
        output.push(value as u8);
        i += offset;
    }

    output
}
