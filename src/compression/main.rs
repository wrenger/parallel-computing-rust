extern crate mpi;

use mpi::traits::{Communicator, Root};

mod bytes;
mod compress;
mod decompress;
mod tree;

const FILE_IN: &str = "resources/lorem_ipsum.txt";
const FILE_COMP: &str = "resources/lorem_ipsum.bin";
const FILE_OUT: &str = "resources/uncompressed.txt";

fn main() {
    let universe = mpi::initialize().unwrap();

    let world = universe.world();
    let rank = world.rank();
    let size = world.size();

    assert!(size >= 2);

    let mut frequencies = compress::count_frequencies(FILE_IN).expect("count freq {}");
    world
        .process_at_rank(0)
        .broadcast_into(&mut frequencies[..]);

    let huff_tree = tree::Tree::from(&mut frequencies);
    let encoding = huff_tree.traverse();

    let time = if rank == 0 { mpi::time() } else { 0.0 };

    match rank {
        0 => compress::manager(FILE_IN, FILE_COMP, world),
        _ => compress::worker(&encoding, world),
    };

    match rank {
        0 => decompress::manager(FILE_COMP, FILE_OUT, world),
        _ => decompress::worker(huff_tree, world),
    };

    if rank == 0 {
        println!("Runtime: {:.6}", mpi::time() - time);
    }
}
