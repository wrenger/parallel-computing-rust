use std::cmp::Ordering;

use super::bytes::bit_at;

pub const SYMBOLS: usize = 128;
pub const READ_IN: usize = 65536;

#[derive(Debug)]
pub enum Tree {
    Empty,
    Inner(Box<Node>),
}

#[derive(Debug)]
pub struct Node {
    freq: u32,
    count: u32,
    pub value: char,
    pub left: Tree,
    pub right: Tree,
}

impl Node {
    #[inline]
    pub fn is_leaf(&self) -> bool {
        self.count == 1
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Encoding {
    pub value: u32,
    pub size: u8,
}

impl Encoding {
    #[inline]
    pub fn new(value: u32, size: u8) -> Encoding {
        Encoding {
            value: value,
            size: size,
        }
    }
}

impl PartialEq for Encoding {
    fn eq(&self, other: &Encoding) -> bool {
        self.value == other.value && self.size == other.size
    }
}

impl Tree {
    #[inline]
    fn new(freq: u32, count: u32, value: char, left: Tree, right: Tree) -> Tree {
        Tree::Inner(Box::new(Node {
            freq: freq,
            count: count,
            value: value,
            left: left,
            right: right,
        }))
    }

    #[inline]
    fn leaf(freq: u32, value: char) -> Tree {
        Tree::new(freq, 1, value, Tree::Empty, Tree::Empty)
    }

    fn combine(left: Tree, right: Tree) -> Tree {
        let mut freq = 0;
        let mut count = 0;
        let mut value = 0 as char;
        let is_left = if let Tree::Inner(ref left_n) = left {
            freq = left_n.freq;
            count = left_n.count;
            value = left_n.value;
            true
        } else {
            false
        };
        let is_right = if let Tree::Inner(ref right_n) = right {
            freq += right_n.freq;
            count += right_n.count;
            true
        } else {
            false
        };

        if is_left && is_right {
            Tree::new(freq, count, value, left, right)
        } else if is_left {
            left
        } else {
            right
        }
    }

    #[inline]
    pub fn traverse(&self) -> [Encoding; SYMBOLS] {
        let mut encoding = [Encoding::new(0, 0); SYMBOLS];
        self.traverse_impl(&mut encoding, 0, 0);
        encoding
    }

    fn traverse_impl(&self, encoding: &mut [Encoding], path: u32, depth: u8) {
        match *self {
            Tree::Inner(ref node) => {
                if node.is_leaf() {
                    encoding[node.value as usize] = Encoding::new(path, depth);
                } else {
                    let path = path << 1;
                    node.left.traverse_impl(encoding, path, depth + 1);
                    node.right.traverse_impl(encoding, path | 1, depth + 1);
                }
            }
            _ => {}
        }
    }

    #[inline]
    pub fn value_of(&self, bytes: &[u8], i: usize) -> (char, usize) {
        self.value_of_impl(bytes, i, 0)
    }

    fn value_of_impl(&self, bytes: &[u8], i: usize, depth: usize) -> (char, usize) {
        if let Tree::Inner(ref node) = *self {
            if node.is_leaf() {
                (node.value, depth)
            } else {
                if bit_at(bytes, i) {
                    node.right.value_of_impl(bytes, i + 1, depth + 1)
                } else {
                    node.left.value_of_impl(bytes, i + 1, depth + 1)
                }
            }
        } else {
            (0 as char, 0)
        }
    }
}

impl Clone for Tree {
    fn clone(&self) -> Tree {
        match *self {
            Tree::Empty => Tree::Empty,
            Tree::Inner(ref boxed) => Tree::new(
                boxed.freq,
                boxed.count,
                boxed.value,
                boxed.left.clone(),
                boxed.right.clone(),
            ),
        }
    }
}

impl Eq for Tree {}

impl PartialEq for Tree {
    fn eq(&self, other: &Tree) -> bool {
        match *self {
            Tree::Inner(ref node) => match other {
                Tree::Inner(ref other) => {
                    node.freq == other.freq
                        && node.count == other.count
                        && node.value == other.value
                        && node.left == other.left
                        && node.right == other.right
                }
                Tree::Empty => false,
            },
            Tree::Empty => match other {
                Tree::Inner(_) => false,
                Tree::Empty => true,
            },
        }
    }
}

impl Ord for Tree {
    fn cmp(&self, other: &Tree) -> Ordering {
        match *self {
            Tree::Inner(ref node) => match other {
                Tree::Inner(ref other) => node.freq.cmp(&other.freq),
                _ => Ordering::Greater,
            },
            _ => match other {
                Tree::Inner(_) => Ordering::Less,
                _ => Ordering::Equal,
            },
        }
    }
}

impl PartialOrd for Tree {
    fn partial_cmp(&self, other: &Tree) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> From<&'a mut [u32; SYMBOLS]> for Tree {
    fn from(frequencies: &mut [u32; SYMBOLS]) -> Self {
        frequencies['@' as usize] += 1;

        let mut tree = vec![];
        for i in 0..SYMBOLS {
            if frequencies[i] > 0 {
                let new = Tree::leaf(frequencies[i], (i as u8).into());
                tree.insert_ordered(new);
            }
        }

        while tree.len() > 1 {
            let left = tree.remove(0);
            let right = tree.remove(0);
            tree.insert_ordered(Tree::combine(left, right));
        }
        tree.remove(0)
    }
}

trait VecOrdered<T: Ord> {
    fn insert_ordered(&mut self, element: T);
}

impl<T: Ord> VecOrdered<T> for Vec<T> {
    fn insert_ordered(&mut self, element: T) {
        match self.binary_search(&element) {
            Err(pos) => self.insert(pos, element),
            Ok(pos) => self.insert(pos, element),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn tree_from_5() {
        let mut frequencies = [0 as u32; SYMBOLS];
        frequencies['a' as usize] = 5;
        frequencies['b' as usize] = 4;
        frequencies['c' as usize] = 3;
        frequencies['d' as usize] = 2;

        let tree = Tree::from(&mut frequencies);

        assert_eq!(
            tree,
            Tree::new(
                15,
                5,
                '@',
                Tree::new(
                    6,
                    3,
                    '@',
                    Tree::new(3, 2, '@', Tree::leaf(1, '@'), Tree::leaf(2, 'd')),
                    Tree::leaf(3, 'c')
                ),
                Tree::new(9, 2, 'b', Tree::leaf(4, 'b'), Tree::leaf(5, 'a'))
            )
        );

        let encoding = tree.traverse();

        assert_eq!(encoding['c' as usize], Encoding::new(0b01, 2), "c");
        assert_eq!(encoding['@' as usize], Encoding::new(0b000, 3), "@");
        assert_eq!(encoding['d' as usize], Encoding::new(0b001, 3), "d");
        assert_eq!(encoding['b' as usize], Encoding::new(0b10, 2), "b");
        assert_eq!(encoding['a' as usize], Encoding::new(0b11, 2), "a");

        let buffer = [0b10_001_000, 0b0];
        assert_eq!(tree.value_of(&buffer, 0), ('b', 2));
        assert_eq!(tree.value_of(&buffer, 2), ('d', 3));
        assert_eq!(tree.value_of(&buffer, 5), ('@', 3));
    }

    #[test]
    fn tree_from_4() {
        let mut frequencies = [0 as u32; SYMBOLS];
        frequencies['b' as usize] = 4;
        frequencies['c' as usize] = 3;
        frequencies['d' as usize] = 2;

        let tree = Tree::from(&mut frequencies);

        assert_eq!(
            tree,
            Tree::new(
                10,
                4,
                'b',
                Tree::leaf(4, 'b'),
                Tree::new(
                    6,
                    3,
                    '@',
                    Tree::new(3, 2, '@', Tree::leaf(1, '@'), Tree::leaf(2, 'd')),
                    Tree::leaf(3, 'c')
                )
            )
        );

        let encoding = tree.traverse();

        assert_eq!(encoding['b' as usize], Encoding::new(0b0, 1), "b");
        assert_eq!(encoding['c' as usize], Encoding::new(0b11, 2), "c");
        assert_eq!(encoding['@' as usize], Encoding::new(0b100, 3), "@");
        assert_eq!(encoding['d' as usize], Encoding::new(0b101, 3), "d");

        let buffer = [0b101_11_100, 0b0];
        assert_eq!(tree.value_of(&buffer, 0), ('d', 3));
        assert_eq!(tree.value_of(&buffer, 3), ('c', 2));
        assert_eq!(tree.value_of(&buffer, 5), ('@', 3));
    }
}
