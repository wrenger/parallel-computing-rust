extern crate mpi;

use self::mpi::topology::{Communicator, SystemCommunicator};
use self::mpi::traits::{Destination, Source};

use std::fs::File;
use std::io::{Read, Result, Write};

use super::bytes::i32_to_bytes;
use super::tree::{Encoding, READ_IN, SYMBOLS};

fn count(buffer: &[u8], frequencies: &mut [u32; SYMBOLS]) {
    for byte in buffer {
        frequencies[*byte as usize] += 1;
    }
}

pub fn count_frequencies(file_path: &str) -> Result<[u32; SYMBOLS]> {
    let mut file = File::open(file_path)?;

    let mut frequencies = [0; SYMBOLS];
    let mut buffer = [0; READ_IN];

    loop {
        let n = file.read(&mut buffer[..])?;
        if n == 0 {
            break;
        }
        count(&buffer[..n], &mut frequencies);
    }

    Ok(frequencies)
}

fn own_size(rank: usize, total: usize, chunk_count: usize) -> usize {
    total / chunk_count + (rank < total % chunk_count) as usize
}

pub fn manager(f_in: &str, f_out: &str, world: SystemCommunicator) {
    let time_start = mpi::time();

    let mut file_in = File::open(f_in).expect("open input: {}");
    let mut file_out = File::create(f_out).expect("create comp: {}");

    let world_size = world.size() as usize;
    let chunk_count = world_size - 1;
    let size = file_in.metadata().expect("file meta: {}").len() as usize;

    let time_read = mpi::time();
    {
        let mut input_buffer = vec![0; own_size(0, size, chunk_count) + 2];
        for i in 0..chunk_count {
            let chunk_size = own_size(i, size, chunk_count) + 1;

            let n = file_in
                .read(&mut input_buffer[0..chunk_size - 1])
                .expect("read input: {}");
            input_buffer[n] = '@' as u8;
            world
                .process_at_rank(i as i32 + 1)
                .send(&input_buffer[0..chunk_size])
        }
    }
    println!("time read: {:.6}", mpi::time() - time_read);

    let time_enc = mpi::time();
    let mut output_buffers = vec![vec![]; chunk_count];

    let mut header = vec![0_u8; 8 + chunk_count * 4];
    i32_to_bytes(chunk_count as i32, &mut header[0..]);

    for _ in 0..chunk_count {
        let (output_buffer, status) = world.any_process().receive_vec::<u8>();
        let chunk = status.source_rank() as usize - 1;
        i32_to_bytes(output_buffer.len() as i32, &mut header[8 + 4 * chunk..]);
        output_buffers[chunk] = output_buffer;
    }
    println!("time enc: {:.6}", mpi::time() - time_enc);

    let time_write = mpi::time();
    file_out.write(&header).expect("write header: {}");

    for output in output_buffers {
        file_out.write(&output).expect("write comp: {}");
    }
    println!("time write: {:.6}", mpi::time() - time_write);
    println!("time compression: {:.6}", mpi::time() - time_start);
}

pub fn worker(encoding: &[Encoding], world: SystemCommunicator) {
    let (input, _status) = world.process_at_rank(0).receive_vec::<u8>();

    let output = encode(&input, encoding);

    world.process_at_rank(0).send(&output[..]);
}

fn encode(input: &[u8], encoding: &[Encoding]) -> Vec<u8> {
    let mut output = Vec::with_capacity(input.len());

    let mut bin_exp: i8 = 8;
    let mut full_byte: u8 = 0b0;

    for byte in input {
        let enc = encoding[*byte as usize];

        bin_exp -= enc.size as i8;
        while bin_exp < 0 {
            full_byte |= (enc.value >> -bin_exp) as u8;
            output.push(full_byte);
            bin_exp += 8;
            full_byte = 0b0;
        }

        full_byte |= (enc.value << bin_exp) as u8;
    }
    output.push(full_byte);

    output
}
